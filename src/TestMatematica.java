import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestMatematica {

    @Test
    public void testSoma() {
        assertEquals(11, Matematica.add(10, 1), 0);
    }

    @Test
    public void testMultiplica() {
        assertEquals(100, Matematica.times(10, 10), 0);
    }
}
